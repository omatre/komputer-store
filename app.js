import { getAllComputers } from './api/computer.js';

function App() {
    this.elFeatures = document.getElementById('laptop-features');
    this.elLaptopImage = document.getElementById('laptop-image');
    this.elComputerList = document.getElementById('laptop-list-dropdown');
    this.elLaptopHeadline = document.getElementById('laptop-headline');
    this.elLaptopDescription = document.getElementById('laptop-description');
    this.elLaptopPrize = document.getElementById('laptop-price');
    this.elGetLoanButton = document.getElementById('get-a-loan');
    this.elPayLoanButton = document.getElementById('pay-loan');
    this.elInsertBankButton = document.getElementById('insert-bank');
    this.elWorkButton = document.getElementById('work-money');
    this.elBuyNowButton = document.getElementById('buy-now');
    this.elBalanceText = document.getElementById('balance-amount');
    this.elPayText = document.getElementById('income-money');
    this.elStatusPaymentText = document.getElementById('status-payment');

    this.computers = [];
    this.loan = 0;
    this.money = 0;
    this.payed = 0;
    this.income = 1000;
    this.loanAmount = 5000;
    this.payBackAmount = 1000;
    this.insertAmount = 1000;
    this.selectedComputer = {
        id: null,
        name: null,
        features: null,
        img: null,
        prize: null,
        description: null
    };

    return this;
}

App.prototype.init = async function () {
    try {
        this.computers = await getAllComputers();
    } catch (error) {

    }

    for (const computer of this.computers) {
        if (computer.hasOwnProperty('name')) {
            const elComputer = document.createElement('option');
            elComputer.innerText = computer.name;
            this.elComputerList.appendChild(elComputer);
        }
    }

    this.elComputerList.onchange = this.handleChange.bind(this);
    this.elGetLoanButton.onclick = this.getALoan.bind(this);
    this.elPayLoanButton.onclick = this.payLoan.bind(this);
    this.elInsertBankButton.onclick = this.insertBank.bind(this);
    this.elWorkButton.onclick = this.getPayed.bind(this);
    this.elBuyNowButton.onclick = this.buyNow.bind(this);

    this.render();
}

App.prototype.buyNow = function (e) {
    if (this.money >= parseInt(this.selectedComputer.prize.replace(' ', ''))) {
        this.money -= parseInt(this.selectedComputer.prize.replace(' ', ''));
        this.elStatusPaymentText.innerText = 'Laptop bought!';
        this.elStatusPaymentText.style = 'color: green;';
    } else {
        this.elStatusPaymentText.innerText = `Not sufficient funds, buddy...`;
        this.elStatusPaymentText.style = 'color: red;';
    }

    this.render();
}

App.prototype.getALoan = function (e) {
    this.loan += this.loanAmount;
    this.money += this.loanAmount;

    this.render();
}

App.prototype.payLoan = function (e) {
    if (this.loan >= this.payBackAmount && this.money >= this.payBackAmount) {
        this.loan -= this.payBackAmount;
        this.money -= this.payBackAmount;
    } else if (this.money >= this.loan) {
        this.loan = 0;
        this.money -= this.loan;
    } else if (this.money == 0){
        this.loan -= this.money;
        this.money -= this.loan;
    }

    this.render();
}

App.prototype.insertBank = function (e) {
    if (this.payed >= this.insertAmount) {
        this.money += this.insertAmount;
        this.payed -= this.insertAmount;
    } else {
        this.money += this.payed;
        this.payed = 0;
    }

    this.render();
}

App.prototype.getPayed = function (e) {
    this.payed += this.income;

    this.render();
}

App.prototype.handleChange = function (e) {
    this.selectedComputer = this.computers[this.elComputerList.selectedIndex - 1];
    this.render();
}

App.prototype.render = function () {
    if (this.selectedComputer == undefined || this.selectedComputer.name == null) {
        this.elComputerList.selectedIndex = 1;
        this.selectedComputer = this.computers[0];
    }

    // const featuresList = this.selectedComputer.features.split(', ');
    this.elFeatures.innerText = this.selectedComputer.features;
    // featuresList.forEach(element => {
    //     this.elFeatures.innerText += `
    //         ${ element}`;
    // });

    this.elLaptopImage.setAttribute('src', this.selectedComputer.img);
    this.elLaptopImage.setAttribute('height', '150em');

    this.elLaptopHeadline.innerText = this.selectedComputer.name;
    this.elLaptopDescription.innerText = this.selectedComputer.description;
    this.elLaptopPrize.innerText = `${this.selectedComputer.prize} NOK`;

    this.elBalanceText.innerText = `${ this.money } NOK`;
    this.elPayText.innerText = `${ this.payed } NOK`;
}

new App().init();